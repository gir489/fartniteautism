﻿using System.IO;
using System.Windows.Forms;

namespace FortniteAutism
{
    public partial class FortniteAutism : Form
    {
        private string workingFileLocation;
        private string notWorkingFileLocation;

        public FortniteAutism()
        {
            InitializeComponent();
        }

        private void btnWorking_Click(object sender, System.EventArgs e)
        {
            OpenFileDialog fileDialog = new OpenFileDialog();
            fileDialog.Filter = "Fortnite Save|*.replay";
            fileDialog.RestoreDirectory = true;
            if (fileDialog.ShowDialog() == DialogResult.OK)
            {
                workingFileLocation = fileDialog.FileName;
            }
        }

        private void btnNotWorking_Click(object sender, System.EventArgs e)
        {
            OpenFileDialog fileDialog = new OpenFileDialog();
            fileDialog.Filter = "Fortnite Save|*.replay";
            fileDialog.RestoreDirectory = true;
            if (fileDialog.ShowDialog() == DialogResult.OK)
            {
                notWorkingFileLocation = fileDialog.FileName;
            }
        }

        private void btnAutism_Click(object sender, System.EventArgs e)
        {
            byte[] workingFile = File.ReadAllBytes(workingFileLocation); 
            byte[] notWorkingFile = File.ReadAllBytes(notWorkingFileLocation);
            notWorkingFile[0x10] = workingFile[0x10];
            notWorkingFile[0x11] = workingFile[0x11];
            notWorkingFile[0x12] = workingFile[0x12];
            File.WriteAllBytes(notWorkingFileLocation, notWorkingFile);
            MessageBox.Show("Completed");
        }
    }
}
