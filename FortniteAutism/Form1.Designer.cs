﻿namespace FortniteAutism
{
    partial class FortniteAutism
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.Button btnWorking;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FortniteAutism));
            this.btnNotWorking = new System.Windows.Forms.Button();
            this.btnAutism = new System.Windows.Forms.Button();
            btnWorking = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnWorking
            // 
            btnWorking.Location = new System.Drawing.Point(35, 12);
            btnWorking.Name = "btnWorking";
            btnWorking.Size = new System.Drawing.Size(75, 23);
            btnWorking.TabIndex = 0;
            btnWorking.Text = "Working";
            btnWorking.UseVisualStyleBackColor = true;
            btnWorking.Click += new System.EventHandler(this.btnWorking_Click);
            // 
            // btnNotWorking
            // 
            this.btnNotWorking.Location = new System.Drawing.Point(116, 12);
            this.btnNotWorking.Name = "btnNotWorking";
            this.btnNotWorking.Size = new System.Drawing.Size(75, 23);
            this.btnNotWorking.TabIndex = 1;
            this.btnNotWorking.Text = "Not Working";
            this.btnNotWorking.UseVisualStyleBackColor = true;
            this.btnNotWorking.Click += new System.EventHandler(this.btnNotWorking_Click);
            // 
            // btnAutism
            // 
            this.btnAutism.Location = new System.Drawing.Point(75, 41);
            this.btnAutism.Name = "btnAutism";
            this.btnAutism.Size = new System.Drawing.Size(75, 23);
            this.btnAutism.TabIndex = 2;
            this.btnAutism.Text = "Autism";
            this.btnAutism.UseVisualStyleBackColor = true;
            this.btnAutism.Click += new System.EventHandler(this.btnAutism_Click);
            // 
            // FortniteAutism
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(239, 80);
            this.Controls.Add(this.btnAutism);
            this.Controls.Add(this.btnNotWorking);
            this.Controls.Add(btnWorking);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximumSize = new System.Drawing.Size(255, 119);
            this.MinimumSize = new System.Drawing.Size(255, 119);
            this.Name = "FortniteAutism";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "A U T I S M";
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button btnNotWorking;
        private System.Windows.Forms.Button btnAutism;
    }
}

